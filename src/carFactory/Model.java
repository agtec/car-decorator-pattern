package carFactory;

public enum Model {

    C1 (1, "C1"),
    C2 (2, "C2"),
    C3 (3, "C3"),
    C4 (4, "C4");

    private int number;
    private String name;

    Model(int number, String name) {
        this.number = number;
        this.name = name;
    }

    public int getNumber() {
        return number;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return getName();
    }
}
