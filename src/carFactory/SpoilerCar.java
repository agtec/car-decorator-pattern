package carFactory;

public class SpoilerCar extends CarDecorator {

    private String spoilerType;

    public SpoilerCar(Car car, String spoilerType) {
        super(car);
        this.spoilerType = spoilerType;
    }

    public void showFeatures(){
        super.showFeatures();
        System.out.println("Spoiler type: " + spoilerType);
    }
}
