package carFactory;

public class WheelsCar extends CarDecorator {

    private String wheelsSize;

    public WheelsCar(Car car, String wheelsSize) {
        super(car);
        this.wheelsSize = wheelsSize;
    }

    public void showFeatures(){
        super.showFeatures();
        System.out.println("Wheels size: " + wheelsSize);
    }
}
