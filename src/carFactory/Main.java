package carFactory;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        Car car = new Citroen();
        int modelNumber = 0;

        do {
            System.out.println("Citroen configurator");
            System.out.println("Choose model: ");
            System.out.println("1 " + Model.C1.getName());
            System.out.println("2 " + Model.C2.getName());
            System.out.println("3 " + Model.C3.getName());
            System.out.println("4 " + Model.C4.getName());
            modelNumber = sc.nextInt();
            sc.nextLine();
        } while (modelNumber < 1 || modelNumber > 4);

        for (Model model : Model.values()) {
            if (modelNumber == model.getNumber()) {
                ((Citroen) car).setModel(model);
            }
        }

        System.out.println("Choose colour: (black/white/blue/red) or press enter to go next: ");
        String colour = sc.nextLine();
        if (colour.length() > 0) {
            Car colouredCar = new ColouredCar(car, colour);
            car = colouredCar;
        }

        System.out.println("Choose wheels size: (15/16/17) or press enter to go next: ");
        String wheelsSize = sc.nextLine();
        if (wheelsSize.length() > 0) {
            WheelsCar car1 = new WheelsCar(car, wheelsSize);
            car = car1;
        }

        System.out.println("Choose spoiler parameters (small/large) or press enter to go next: ");
        String spoilerSize = sc.nextLine();
        if (spoilerSize.length() > 0) {
            SpoilerCar car2 = new SpoilerCar(car, spoilerSize);
            car = car2;
        }

        System.out.println("You chose: ");
        car.showFeatures();

    }
}
