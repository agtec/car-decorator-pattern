package carFactory;

public class ColouredCar extends CarDecorator {

    private String color;

    public ColouredCar(Car car, String color) {
        super(car);
        this.color = color;
    }

    public void showFeatures(){
        super.showFeatures();
        System.out.println("Colour: " + color);
    }
}
