package carFactory;

public class Citroen implements Car {

    private Model model;

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    @Override
    public void showFeatures() {
        System.out.println("Citroen: " + getModel());
    }
}
